<?php
/**
 * Функиция транслитерации
 * @param string
 * @return string
 */
function translit($value)
{
    $converter = array(
        'а' => 'a',    'б' => 'b',    'в' => 'v',    'г' => 'g',    'д' => 'd',
        'е' => 'e',    'ё' => 'e',    'ж' => 'zh',   'з' => 'z',    'и' => 'i',
        'й' => 'y',    'к' => 'k',    'л' => 'l',    'м' => 'm',    'н' => 'n',
        'о' => 'o',    'п' => 'p',    'р' => 'r',    'с' => 's',    'т' => 't',
        'у' => 'u',    'ф' => 'f',    'х' => 'h',    'ц' => 'c',    'ч' => 'ch',
        'ш' => 'sh',   'щ' => 'sch',  'ь' => '',     'ы' => 'y',    'ъ' => '',
        'э' => 'e',    'ю' => 'yu',   'я' => 'ya',

        'А' => 'A',    'Б' => 'B',    'В' => 'V',    'Г' => 'G',    'Д' => 'D',
        'Е' => 'E',    'Ё' => 'E',    'Ж' => 'Zh',   'З' => 'Z',    'И' => 'I',
        'Й' => 'Y',    'К' => 'K',    'Л' => 'L',    'М' => 'M',    'Н' => 'N',
        'О' => 'O',    'П' => 'P',    'Р' => 'R',    'С' => 'S',    'Т' => 'T',
        'У' => 'U',    'Ф' => 'F',    'Х' => 'H',    'Ц' => 'C',    'Ч' => 'Ch',
        'Ш' => 'Sh',   'Щ' => 'Sch',  'Ь' => '',     'Ы' => 'Y',    'Ъ' => '',
        'Э' => 'E',    'Ю' => 'Yu',   'Я' => 'Ya',
    );

    $value = strtr($value, $converter);
    return $value;
}
  function processingLoginUser() {
      // если session login и password есть

      if($_SERVER['REQUEST_METHOD'] == 'GET') {
          /*
           *  заполняем values ранее введенными значениями из БД <=> нет errors,
           *  иначе заполняем значениями из cookies и показываем errors
          */
          $messages = array();
          // Если есть параметр save, то выводим сообщение пользователю.
          // cookie для логина и пароля. удаление cookie
          setcookie('login', '', 100000);
          setcookie('pass', '', 100000);
          if (!empty($_COOKIES['save'])) {
              // Удаляем куку, указывая время устаревания в прошлом.
              setcookie('save', '', 100000);
              // Если есть параметр save, то выводим сообщение пользователю.
              // cookie для логина и пароля. удаление cookie
              $messages[] = 'Спасибо, результаты сохранены.';
              /* XSS */
              if (!empty($_COOKIE['pass'])) {
                  $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
                      htmlspecialchars($_COOKIE['login']),
                      htmlspecialchars($_COOKIE['pass']));
              }
          }
          $errors = array();
          $errors['name_empty'] = !empty($_COOKIE['name_error_empty']);
          $errors['name_mistype'] = !empty($_COOKIE['name_error_mistype']);
          $errors['email'] = !empty($_COOKIE['email_error']);
          $errors['birth'] = !empty($_COOKIE['birth_error']);
          $errors['gender'] = !empty($_COOKIE['gender_error']);
          $errors['counter'] = !empty($_COOKIE['counter_error']);
          $errors['options'] = !empty($_COOKIE['options_error']);
          $errors["info"] = !empty($_COOKIE['info_error']);
          $errors["checked"] = !empty($_COOKIE['checked_error']);
          $errors['name'] = $errors['name_empty'] || $errors['name_mistype'];

          if ($errors['name_empty']) {
              // Удаляем куку, указывая время устаревания в прошлом.
              setcookie('name_error_empty', "", 100000);
              // Выводим сообщение.
              $messages[] = '<div class="error">Заполните имя.</div>';
          }
          if ($errors['name_mistype']) {
              // Удаляем куку, указывая время устаревания в прошлом.
              setcookie('name_error_mistype', "", 100000);
              // Выводим сообщение.
              $messages[] = "<div class='error'>Имя должно состоять только из русский или английских букв и пробелов</div>";
          }
          if ($errors['email']) {
              setcookie('email_error', "", 100000);
              $messages[] = '<div class="error">Заполните поле email.</div>';
          }
          if ($errors['birth']) {
              setcookie('birth_error', "", 100000);
              $messages[] = "<div class='error'>Заполните поле дата рождения</div>";
          }
          if ($errors['gender']) {
              setcookie('gender_error', "", 100000);
              $messages[] = "<div class='error'>Выберете пол</div>";
          }
          if ($errors['counter']) {
              setcookie('counter_error', "", 100000);
              $messages[] = "<div class='error'>Выберете количество конечностей</div>";
          }
          if ($errors['options']) {
              setcookie('options_error', "", 100000);
              $messages[] = "<div class='error'>А где ваши способности?</div>";
          }
          if ($errors['info']) {
              setcookie('info_error', "", 100000);
              $messages[] = "<div class='error'>Кто вы?Поподробнее</div>";
          }
          if ($errors["checked"]) {
              setcookie('checked_error', "", 100000);
              $messages[] = "<div class='error'>Согласитесь с нами</div>";
          }

          // Складываем предыдущие значения полей в массив, если есть.
          // При этом санитизуем все данные для безопасного отображения в браузере.
          /*  XSS */
          $values = array();
          $values['name'] = empty($_COOKIE['name_value']) ? '' : strip_tags($_COOKIE['name_value']);
          $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
          $values['birth'] = empty($_COOKIE['birth_value']) ? '' : strip_tags($_COOKIE['birth_value']);
          $values['gender'] = empty($_COOKIE['gender_value']) ? '' : strip_tags($_COOKIE['gender_value']);
          $values['counter'] = empty($_COOKIE['counter_value']) ? '' : strip_tags($_COOKIE['counter_value']);
          $options_value = array();
          $options_value['nodeath'] = FALSE;
          $options_value['through'] = FALSE;
          $options_value['levitation'] = FALSE;
          $values['options'] = empty($_COOKIE['options_value']) ? $options_value : json_decode($_COOKIE['options_value'], true);
          $values['info'] = empty($_COOKIE['info_value']) ? '' : strip_tags($_COOKIE['info_value']);
          $values['checked'] = empty($_COOKIE['checked_value']) ? '' : strip_tags($_COOKIE['checked_value']);

          if (empty($errors)) {
              /*сбор данных из БД*/

              printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
              $user = 'u20386';
              $pass = '2551027';
              $db = new PDO('mysql:host=localhost;dbname=u20386', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
              try {
                  $stmt = $db->prepare("SELECT name, email, year, gender, lungs, nodeath, through, levitation, biography FROM app WHERE  id=:uid");
                  $stmt->execute();
                  $row = $stmt->fetch();
                  /* XSS */
                  $values['name'] = strip_tags($row['name']);
                  $values['email'] = strip_tags($row['email']);
                  $values['birth'] = strip_tags($row['year']);
                  $values['gender'] = strip_tags($row['gender']);
                  $values['counter'] = strip_tags($row['lungs']);
                  $options_value = array();
                  $options_value['nodeath'] = strip_tags($row['nodeath']) == 1;
                  $options_value['through'] = strip_tags($row['through']) == 1;
                  $options_value['levitation'] = strip_tags($row['levitation']) == 1;
                  $values['options'] = $options_value;
                  $values['info'] = strip_tags($row['biography']);

              } catch (PDOException $e) {
                  print('Error : ' . $e->getMessage());
                  exit();

              }


          }
          include('form.php');
          // Завершаем работу скрипта.
          exit();
      }
          else {
              /*
                перезаписываем данные из БД, зная uid - id юзера в БД <=> нет ошибок
                иначе сохраняем ошибки в кукис
              */
              $errors = FALSE;
              $flag = array();
              $flag["name"] = TRUE;
              $flag["email"] = TRUE;
              $flag["birth"] = TRUE;
              $flag["gender"] = TRUE;
              $flag["counter"]= TRUE;
              $flag["options"]= TRUE;
              $flag["info"] = TRUE;
              $flag["checked"] = TRUE;

              if (empty($_POST['name']) || strlen($_POST['name']) > 128) {
                  setcookie('name_error_empty', '1', time() + 24 * 60 * 60);
                  $errors = TRUE;
                  $flag["name"] = FALSE;
              }
              if(preg_match('/[^а-яА-Яa-zA-Z\s]+/musi',$_POST['name'])) {
                  setcookie('name_error_mistype', '1', time() + 24 * 60 * 60);
                  $errors = TRUE;

                  $flag["name"] = FALSE;
              }
              if (empty(trim($_POST['email']))) {
                  setcookie('email_error', '1', time() + 24 * 60 * 60);
                  $errors = TRUE;
                  $flag["email"]= FALSE;
              }
              if (empty(trim($_POST['birth']))) {
                  setcookie('birth_error', '1',  time() + 24 * 60 * 60);
                  $errors = TRUE;
                  $flag["birth"] = FALSE;
              }
              if (!isset($_POST['gender'])) {
                  setcookie('gender_error', '1',  time() + 24 * 60 * 60);
                  $flag["gender"] = FALSE;
                  $errors = TRUE;
              }
              if (!isset($_POST['counter'])) {
                  setcookie('counter_error', '1',  time() + 24 * 60 * 60);
                  $errors = TRUE;
                  $flag["counter"] = FALSE;
              }
              if (!isset($_POST["options"])) {
                  setcookie('options_error', '1',  time() + 24 * 60 * 60);
                  $errors = TRUE;
                  $flag["options"] = FALSE;
              }
              if (empty(trim($_POST["info"]))) {
                  setcookie('info_error', '1',  time() + 24 * 60 * 60);
                  $errors = TRUE;
                  $flag["info"] = FALSE;
              }
              if(!isset($_POST["checked"])) {
                  setcookie('checked_error', '1',  time() + 24 * 60 * 60);
                  $errors = TRUE;
                  $flag["checked"] = FALSE;
              }
              /* заполняем в кукис правильно введенные поля */
              if($flag["name"]) {
                  setcookie('name_value', $_POST['name'], time() + 24 * 60 * 60);
              }
              if($flag["email"]) {
                  setcookie('email_value', $_POST['email'], time() + 24 * 60 * 60);
              }
              if($flag["birth"]) {
                  setcookie('birth_value', $_POST['birth'], time() + 24 * 60 * 60);
              }
              if($flag["gender"]){
                  setcookie('gender_value', $_POST['gender'],  time() + 24 * 60 * 60);
              }
              if($flag["counter"]){
                  setcookie("counter_value", $_POST["counter"], time() + 24 * 60 * 60);
              }
              if($flag["options"]){
                  $options_cookie = array();
                  $options_cookie["nodeath"] = in_array("nodeath",$_POST["options"]);
                  $options_cookie["through"] = in_array("through",$_POST["options"]);
                  $options_cookie["levitation"] = in_array("levitation", $_POST["options"]);
                  setcookie("options_value", json_encode($options_cookie), time() + 24 * 60 * 60);
              }
              if($flag["info"]) {
                  setcookie("info_value", $_POST["info"], time() + 24 * 60 * 60);
              }
              if($flag["checked"]) {
                  setcookie("checked_value", TRUE, time() + 24 * 60 * 60);
              }
              if ($errors) {
                  // При наличии ошибок завершаем работу скрипта.
                  header('Location: index.php');
                  exit();
              }
              else  {
                  // ошибок нет => обнуляем cookies с ошибками
                  setcookie('name_error_empty', '', 100000);
                  setcookie('name_error_mistype', '', 100000);
                  setcookie('email_error', '', 100000);
                  setcookie('birth_error', '', 100000);
                  setcookie('gender_error', '', 100000);
                  setcookie('counter_error', '', 100000);
                  setcookie('options_error', '', 100000);
                  setcookie('info_error', '', 100000);
                  setcookie('checked_error', '', 100000);

                  try {
                      /* XSS */
                     
                      $user = 'u20386';
                      $pass = '2551027';
                      $db = new PDO('mysql:host=localhost;dbname=u20386', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
                      $stmt = $db->prepare("UPDATE app SET 
                                               name=:name,
                                               email=:email, 
                                               year=:year, 
                                               gender=:gender, 
                                               lungs=:lungs, 
                                               nodeath=:nodeath, 
                                               through=:through, 
                                               levitation=:levitation,
                                               biography=:biography
                                         WHERE  id=:uid");
                      $uid = $_SESSION['uid'];

                      $stmt->bindParam(':uid', $uid, PDO::PARAM_INT);

                      $name = $_POST['name'];
                      $stmt->bindParam(':name', $name, PDO::PARAM_STR);
                      $email = $_POST['email'];
                      $stmt->bindParam(':email', $email, PDO::PARAM_STR);
                      $birth = $_POST['birth'];
                      $stmt->bindParam(':year', $birth, PDO::PARAM_INT);
                      $gender = $_POST['gender'];
                      $stmt->bindParam(':gender', $gender, PDO::PARAM_INT);
                      $counter = $_POST['counter'];
                      $stmt->bindParam(':lungs', $counter, PDO::PARAM_INT);
                      $options = $_POST['options'];
                      $nodeath = in_array("nodeath", $options);
                      $stmt->bindParam(':nodeath', $nodeath, PDO::PARAM_INT);
                      $through = in_array("through", $options);
                      $stmt->bindParam(':through', $through, PDO::PARAM_INT);
                      $levitation = in_array("levitation", $options);
                      $stmt->bindParam(':levitation', $levitation, PDO::PARAM_INT);
                      $info = $_POST['info'];
                      $stmt->bindParam(':biography', $info, PDO::PARAM_STR);
                      $stmt->execute();

              }
                  catch(PDOException $e){
                      print('Error : ' . $e->getMessage());
                      exit();
                  }
                 header('Location: ./');
          }
      }
  }
  function processingWillRegisterUser() {
      if($_SERVER['REQUEST_METHOD'] == 'GET') {
          if (!empty($_COOKIE['save'])) {
              // Удаляем куку, указывая время устаревания в прошлом.
              setcookie('save', '', 100000);
              // Если есть параметр save, то выводим сообщение пользователю.
              // cookie для логина и пароля. удаление cookie
              setcookie('login', '', 100000);
              setcookie('pass', '', 100000);
              $messages[] = 'Спасибо, результаты сохранены.';
              // Если в куках есть пароль, то выводим сообщение.
              if (!empty($_COOKIE['pass'])) {
                  $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
                      strip_tags($_COOKIE['login']),
                      strip_tags($_COOKIE['pass']));
              }


          }
          $errors = array();
          $errors['name_empty'] = !empty($_COOKIE['name_error_empty']);
          $errors['name_mistype'] = !empty($_COOKIE['name_error_mistype']);
          $errors['email'] = !empty($_COOKIE['email_error']);
          $errors['birth'] = !empty($_COOKIE['birth_error']);
          $errors['gender'] = !empty($_COOKIE['gender_error']);
          $errors['counter'] = !empty($_COOKIE['counter_error']);
          $errors['options'] = !empty($_COOKIE['options_error']);
          $errors["info"] = !empty($_COOKIE['info_error']);
          $errors["checked"] = !empty($_COOKIE['checked_error']);
          $errors['name'] = $errors['name_empty'] || $errors['name_mistype'];

          if ($errors['name_empty']) {
              // Удаляем куку, указывая время устаревания в прошлом.
              setcookie('name_error_empty', "", 100000);
              // Выводим сообщение.
              $messages[] = '<div class="error">Заполните имя.</div>';
          }
          if ($errors['name_mistype']) {
              // Удаляем куку, указывая время устаревания в прошлом.
              setcookie('name_error_mistype', "", 100000);
              // Выводим сообщение.
              $messages[] = "<div class='error'>Имя должно состоять только из русский или английских букв и пробелов</div>";
          }
          if ($errors['email']) {
              setcookie('email_error', "", 100000);
              $messages[] = '<div class="error">Заполните поле email.</div>';
          }
          if ($errors['birth']) {
              setcookie('birth_error', "", 100000);
              $messages[] = "<div class='error'>Заполните поле дата рождения</div>";
          }
          if ($errors['gender']) {
              setcookie('gender_error', "", 100000);
              $messages[] = "<div class='error'>Выберете пол</div>";
          }
          if ($errors['counter']) {
              setcookie('counter_error', "", 100000);
              $messages[] = "<div class='error'>Выберете количество конечностей</div>";
          }
          if ($errors['options']) {
              setcookie('options_error', "", 100000);
              $messages[] = "<div class='error'>А где ваши способности?</div>";
          }
          if ($errors['info']) {
              setcookie('info_error', "", 100000);
              $messages[] = "<div class='error'>Кто вы?Поподробнее</div>";
          }
          if ($errors["checked"]) {
              setcookie('checked_error', "", 100000);
              $messages[] = "<div class='error'>Согласитесь с нами</div>";
          }

          // Складываем предыдущие значения полей в массив, если есть.
          // При этом санитизуем все данные для безопасного отображения в браузере. 
          /* XSS */

          $values = array();
          $values['name'] = empty($_COOKIE['name_value']) ? '' : strip_tags($_COOKIE['name_value']);
          $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
          $values['birth'] = empty($_COOKIE['birth_value']) ? '' : strip_tags($_COOKIE['birth_value']);
          $values['gender'] = empty($_COOKIE['gender_value']) ? '' : strip_tags($_COOKIE['gender_value']);
          $values['counter'] = empty($_COOKIE['counter_value']) ? '' : strip_tags($_COOKIE['counter_value']);
          $options_value = array();
          $options_value['nodeath'] = FALSE;
          $options_value['through'] = FALSE;
          $options_value['levitation'] = FALSE;
          $values['options'] = empty($_COOKIE['options_value']) ? $options_value : json_decode($_COOKIE['options_value'], true);
          $values['info'] = empty($_COOKIE['info_value']) ? '' : strip_tags($_COOKIE['info_value']);
          $values['checked'] = empty($_COOKIE['checked_value']) ? '' : strip_tags($_COOKIE['checked_value']);
          include('form.php');
          // Завершаем работу скрипта.
          exit();
      }
      else {
          $login = $_POST['email'] . rand();
          if(strlen($login) > 28) {
              $login = substr($login, 0, 28);

          }

          $password = translit($_POST['name']) . rand();
          if(strlen($password) > 28) {
              $password = substr($password, 0, 28);
          }
          // Сохраняем в Cookies.
          setcookie('pass', $password);
          $password = md5($password);
          print(strlen($login));
          print(strlen($password));
          setcookie('login', $login);


          try {
              $user = 'u20386';
              $pass = '2551027';
              $db = new PDO('mysql:host=localhost;dbname=u20386', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
              $stmt = $db->prepare("INSERT INTO app SET name = ?, email = ?, year = ?, gender = ?, lungs = ?, nodeath = ?, through = ?, levitation = ?, biography = ?, login = ?, pass = ? ");
              /* XSS */
              $nodeath = in_array("nodeath",$_POST['options']) ? 1 : 0;
              $through = in_array("through",$_POST['options']) ? 1 : 0;
              $levitation = in_array("levitation", $_POST['options']) ? 1 : 0;
              $name =  (string)$_POST['name'];
              $email = (string)$_POST['email'];
              $birth = (string)$_POST['birth'];
              $gender = (int)$_POST['gender'];
              $counter = (int)$_POST['counter'];
              $info = (string)$_POST['info'];
              $info = (string)$_POST['info'];
             
              $stmt->execute([$name, $email, $birth, $gender, $counter, $nodeath, $through, $levitation,$info, $login, $password ]);

          } catch(PDOException $e){
              print('Error : ' . $e->getMessage());
              exit();
          }
          header('Location: ./');

      }
  }

  if(!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {

      processingLoginUser();
  }
  else {
      processingWillRegisterUser();
  }
  setcookie('save',1 );
